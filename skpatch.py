from skimage import io
from skimage.transform import rotate
from sklearn.feature_extraction import image
from matplotlib import pyplot as plt
#for Saving only
import cv2
import imageio

image_file="images/t0.tif"
# image_file="output/test_1.tif"
t0=imageio.imread(image_file)
io.use_plugin('freeimage')

t_t0=rotate(image=t0, angle=90)
print(type(t_t0))
print(t_t0.shape)
# Saving using cv
cv2.imwrite('cv_t_t0.tif',t_t0)
# imageio.imwrite('test1_1_gray.jpg', t_t0[:,:,0])
io.imshow(t_t0)
# imageio.imwrite('test1_1.tif', t0)
plt.show()
#Each patch is transformed into four angles
degrees=[0.0, 90.0, 180.0, 270.0]
# for deg in degrees:
#     t_t0 = rotate(image=t0, angle=deg)
#     io.imshow(t_t0)
#     plt.show()

# Creating Patches
# patches=image.extract_patches_2d(t0, (512,512), max_patches=5, random_state=0)
# print(type(patches))
# index=0
# for i in range(5):
#     # print(type(patches[i]))
#     print("%d out of 5 Created" % i)
#     for j,degree in enumerate(degrees):
#         index=index+1
#         transformed_image=rotate(image=patches[i], angle=degree)
#         filename='output/test_%d.tif' % index
#         print(filename)
#         # io.imshow(transformed_image)
#         # plt.show()
#         imageio.imwrite(filename, transformed_image)
# print("Patches Created Successfully")
