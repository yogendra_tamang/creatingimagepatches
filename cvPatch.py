from matplotlib import pyplot as plt
import cv2
import math
import numpy as np
from matplotlib import pyplot as plt
# Declare Variables
patch_size=[512, 512]
degrees=[0, 90, 180, 270]
log_factor=1
#Stride is 50%
stride=256
# Reading Image
image_name="images/t0.tif"
image_file=cv2.imread(image_name, cv2.IMREAD_COLOR)
print(type(image_file))

# Creating temp image variable for preprocessing

# hist = cv2.calcHist([image_file],[0],None,[256],[0,256])
color = ('b','g','r')
for i,col in enumerate(color):
    histr = cv2.calcHist([image_file],[i],None,[256],[0,256])
    plt.plot(histr,color = col)
    plt.xlim([0,256])
plt.show()


img_temp = np.array(image_file, dtype=np.uint8)
img_temp = np.array(img_temp, dtype=np.float64)

rows, cols, channel=img_temp.shape
print(rows, cols, channel)
#Show Image
# cv2.imshow('Test',img_temp)
# cv2.waitKey(0)
# Horizontal h and Vertical v Patch Numbers, OpenCV image Format==VerticalxHorizontalxDepth
h=(cols-patch_size[0]//2)//stride
v=(rows-patch_size[1]//2)//stride
print(h, v)
num_of_patches=h*v

def normalizeStaining(I=None):
    # I = np.asarray(Image.open('example1.tif'),dtype=np.float64);
    # if not exist(char('Io'),char('var')) or isempty(Io):
    Io = 240
    # if not exist(char('beta'),char('var')) or isempty(beta):
    beta = 0.15
    # if not exist(char('alpha'),char('var')) or isempty(alpha):
    alpha = 1
    # if not exist(char('HERef'),char('var')) or isempty(HERef):
    # HERef=matlabarray(cat(0.5626,0.2159,0.7201,0.8012,0.4062,0.5581))
    HERef = np.array([[0.5626, 0.2159], [0.7201, 0.8012], [0.4062, 0.5581]])
    # if not exist(char('maxCRef)'),char('var')) or isempty(maxCRef):
    # maxCRef=matlabarray(cat(1.9705,1.0308))
    maxCRef = np.array([1.9705, 1.0308])

    (h, w, c) = np.shape(I)
    # h=size(I,1)
    # w=size(I,2)
    # I=double(I)
    I = np.reshape(I, (h * w, c), order='F')
    OD = - np.log((I + 1) / Io)
    ODhat = (OD[(np.logical_not((OD < beta).any(axis=1))), :])
    # V,__=eig(cov(ODhat),nargout=2)
    (W, V) = np.linalg.eig(np.cov(ODhat, rowvar=0))
    Vec = - np.transpose(np.array([V[:, 1], V[:, 0]]))  # desnecessario o sinal negativo
    That = np.dot(ODhat, Vec)
    phi = np.arctan2(That[:, 1], That[:, 0])
    minPhi = np.percentile(phi, alpha)
    maxPhi = np.percentile(phi, 100 - alpha)
    vMin = np.dot(Vec, np.array([np.cos(minPhi), np.sin(minPhi)]))
    vMax = np.dot(Vec, np.array([np.cos(maxPhi), np.sin(maxPhi)]))
    if vMin[0] > vMax[0]:
        HE = np.array([vMin, vMax])
    else:
        HE = np.array([vMax, vMin])

    HE = np.transpose(HE)
    Y = np.transpose(np.reshape(OD, (h * w, c)))
    #
    #    print(np.shape(HE))
    #    print(np.shape(Y))


    C = np.linalg.lstsq(HE, Y)
    maxC = np.percentile(C[0], 99, axis=1)

    # C=bsxfun(rdivide,C,maxC)
    C = C[0] / maxC[:, None]
    # C=bsxfun(times,C,maxCRef)
    C = C * maxCRef[:, None]
    print("Normalization Step")
    Inorm = Io * np.exp(- np.dot(HERef, C))
    Inorm = np.reshape(np.transpose(Inorm), (h, w, c), order='F')
    Inorm = np.clip(Inorm, 0, 255);
    Inorm = np.array(Inorm, dtype=np.uint8)
    #    if nargout > 1:
    #        H=Io * exp(- HERef[:,1] * C[1,:])
    #        H=reshape(H.T,h,w,3)
    #        H=uint8(H)
    #    if nargout > 2:
    #        E=Io * exp(- HERef[:,2] * C[2,:])
    #        E=reshape(E.T,h,w,3)
    #        E=uint8(E)
    return Inorm  # ,H,E

print("Possible Patches : {0}".format(num_of_patches))
print('Log Transformation')

new_image=normalizeStaining(I=img_temp)

print("Log Transormation Successfull")
cv2.imshow("Log Transformed", new_image)
cv2.waitKey(0)
patches=[]
for j in range(v):
    for i in range(h):
        x_index=i*stride
        y_index=j*stride
        patches.append(new_image[y_index:(y_index+patch_size[1]),x_index:(x_index+patch_size[0]),:])
for i in range(len(patches)):
    for deg in degrees:
        print("Patch===== %d    ||   Degree ======%d" % (i, deg))
        M = cv2.getRotationMatrix2D((patch_size[0] // 2, patch_size[1] // 2), deg, 1)
        dst = cv2.warpAffine(src=patches[i], M=M, dsize=(patch_size[0], patch_size[1]))
        print("Patch===== %d    ||   Degree ======%d Flip vertical" % (i, deg))
        dst_v = cv2.flip(dst, 1)
        cv2.imwrite('output/test_%d_%d.tif' %(i, deg), dst)
        cv2.imwrite('output/test_%d_%d_v.tif' % (i, deg), dst_v)
# print(rows, cols, channel)
# cv2.imshow('DST',dst)
# print(dst.shape)
print("Patches Created Successfully")
